
import utfpr.ct.dainf.if62c.projeto.PoligonalFechada;
import utfpr.ct.dainf.if62c.projeto.Ponto2D;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        Ponto2D p1 = new PontoXZ(-3.2,2);
        Ponto2D p2 = new PontoXZ(-3, 6);
        Ponto2D p3 = new PontoXZ(0, 2);
        
        Ponto2D[] vertices = new Ponto2D[3];
        
        vertices[0] = p1;
        vertices[1] = p2;
        vertices[2] = p3;        
         
        PoligonalFechada pf = new PoligonalFechada(vertices);
        
        System.out.println("Comprimento da poligonal = " + pf.getComprimento());
    }
    
}
