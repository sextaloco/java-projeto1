/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author leandro
 * @param <T>
 */
public class PoligonalFechada<T> extends Poligonal {
    public T[] vertices;
    
    public PoligonalFechada() {}
    
    public PoligonalFechada(T[] vertices) {
        this.vertices = vertices;
    }
    
    @Override
    public double getComprimento() {
   
        double comp = 0;
        
        Ponto p = (Ponto) vertices[0];

        for (T vertice : vertices) {
            comp += p.dist((Ponto) vertice);
            p = (Ponto) vertice;
        }
        
        comp += p.dist((Ponto) vertices[0]);
        return comp;
        
    }
    
}
