/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author leandro
 */
public class PontoXZ extends Ponto2D {
    private double x, z;
    
    public PontoXZ() {}
    
    public PontoXZ(double x, double z) {
        this.x = x;
        this.z = z;
    }
    
    @Override
    public String toString() {
        return String.format(getNome() +"(%f,%f)", getX(), getZ());
    }

    /**
     * @return the x
     */
    @Override
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    @Override
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the z
     */
    @Override
    public double getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    @Override
    public void setZ(double z) {
        this.z = z;
    }


}
