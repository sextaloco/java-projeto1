/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author leandro
 */
public class PontoYZ extends Ponto2D {
    private double y, z;
    
    public PontoYZ() {}
    
    public PontoYZ(double y, double z) {
        this.y = y;
        this.z = z;
    }
    
    @Override
    public String toString() {
        return String.format(getNome() +"(%f,%f)", getY(), getZ());
    }

    /**
     * @return the y
     */
    @Override
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    @Override
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the z
     */
    @Override
    public double getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    @Override
    public void setZ(double z) {
        this.z = z;
    }
}
