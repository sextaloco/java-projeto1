package utfpr.ct.dainf.if62c.projeto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    @Override
    public String toString() {
        return String.format(getNome() + "(%d,%d,%d)", x, y, z);
    }
    
    public boolean equals(Ponto p1) {
        if(p1 instanceof Ponto) {
            if(p1 instanceof PontoXY) {
                return this.getX() == p1.getX() && this.getY() == p1.getY();
            }
            if(p1 instanceof PontoXZ) {
                return this.getX() == p1.getX() && this.getZ() == p1.getZ();
            }
            if(p1 instanceof PontoYZ) {
                return this.getY() == p1.getY() && this.getZ() == p1.getZ();
            }
        } 
        return false;
    }
    
    public double dist(Ponto p1) {
        return Math.sqrt(Math.pow(p1.getX() - this.getX(), 2) + Math.pow(p1.getY() - this.getY(), 2) + Math.pow(p1.getZ() - this.getZ(), 2));
    }
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the z
     */
    public double getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    public void setZ(double z) {
        this.z = z;
    }

}
