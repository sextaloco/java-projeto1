/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author leandro
 * @param <T>
 
 */
public class Poligonal<T> {
    private Ponto2D[] vertices;
    

    /**
     * Construtor padrão
     */
    public Poligonal() {
    }
    
    
    public Poligonal(Ponto2D[] vertices) {
        if(vertices.length < 2 ) {
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        } else {
            this.vertices = vertices;
        }
    }


    /**
     *
     * @return número de vértices do poligonal
     */
    public int getN() {
        return getVertices().length;
    }
    
    public Ponto2D get(int c1) {
        return vertices[c1];
    }
    
    public void set(int c1, Ponto2D vertices) {
        this.vertices[c1] = vertices;
    }
    
    public double getComprimento() {
        double comp = 0;
        
        Ponto p1 = (Ponto) vertices[0];
        for (Ponto2D p2 : vertices) {
            comp += p1.dist((Ponto) p2);
            p1 = (Ponto) p2;
        }
        return comp;
    }
    
    /**
     * @return the vertices
     */
    public Ponto2D[] getVertices() {
        return vertices;
    }

    /**
     * @param vertices the vertices to set
     */
    public void setVertices(Ponto2D[] vertices) {
        this.vertices = vertices;
    }
    
    
}
